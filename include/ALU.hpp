#ifndef ALU_HPP
#define ALU_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(ALU) 
{
  static const unsigned bit_data = 32;

  sc_in<sc_bv<bit_data> > op1;
  sc_in<sc_bv<bit_data> > op2;
  sc_in<sc_bv<3> > control;

  sc_out<bool> zero;
  sc_out<sc_bv<bit_data> > res;
  
  SC_CTOR(ALU) 
  {
    SC_THREAD(behav);
      sensitive << op1 << op2 << control;
  } 
  
 private:
  void behav();
};

#endif
