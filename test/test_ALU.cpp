#include <systemc.h>
#include <iostream>
#include "ALU.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(5, SC_NS);
const unsigned bit_data = 32;
const unsigned numb_test = 8;
const sc_bv<bit_data> theor_res[numb_test] = {0, 1, 3, 4, 2, 3, 1, 0};

SC_MODULE(TestBench) 
{
 public:
  sc_signal<sc_bv<bit_data> > op1;
  sc_signal<sc_bv<bit_data> > op2;
  sc_signal<sc_bv<3> > control;

  sc_signal<bool> zero;
  sc_signal<sc_bv<bit_data> > res;

  ALU alu;

  SC_CTOR(TestBench) : alu("alu")
  {
    SC_THREAD(stimulus_thread);
    SC_THREAD(res_update_and_watcher);
      sensitive << op1 << op2 << control;

    alu.op1(this->op1);
    alu.op2(this->op2);
    alu.control(this->control);
    
	  alu.zero(this->zero);
    alu.res(this->res);
  }

  bool check() 
  {
    bool error = 0;
    for(unsigned i = 0; i < numb_test; i++)
    {
      if(theor_res[i] != results[i])
      {
        cout << "test failed!!\nTheoric result : " << theor_res[i]
             << "\nMy result: " <<  results[i] << endl << endl;
        error = 1;
      }
    }
    return error;
  }
 
 private:
  sc_bv<bit_data> results[numb_test];

  void stimulus_thread() 
  {    
    
    op1.write(1);
    op2.write(0);
    control.write(0);
    wait(wait_time);

    for(unsigned i = 1; i < numb_test; i++)
    {
      if(i%2)
        op2.write((op2.read()).to_uint() + 1);
      else op1.write((op1.read()).to_uint() + 1);

      control.write(i);
      wait(wait_time);
    } 
  }

  void res_update_and_watcher()
  {
    for(unsigned i = 0; i < numb_test; i++)
    {
      wait();
      wait(1, SC_PS);  //to avoid the update of the vector before the execution of the component
      cout << "At time " << sc_time_stamp() << ":\n   ALU's inputs: " << op1.read().to_uint()
      << ", " << op2.read().to_uint() << ", " << control.read()
      <<  "\n   Res of ALU = " << res.read() << endl;

      results[i] = res.read();
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.check();
}
