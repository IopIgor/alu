#include <systemc.h>
#include "ALU.hpp"

using namespace std;
using namespace sc_core;

void ALU::behav()
{
   unsigned temp;
   while(true)
   {
      wait();
      switch ((control->read()).to_uint())
      {
         case 0 : temp = (op1->read() & op2->read()).to_uint(); break; // AND
         case 1 : temp = (op1->read() | op2->read()).to_uint(); break; // OR
         case 2 : temp = (op1->read()).to_uint() + (op2->read()).to_uint(); break; // SUM
         case 3 : temp = (op1->read()).to_uint() + (op2->read()).to_uint(); break; // SUM
         case 4 : temp = (op1->read() & op2->read()).to_uint(); break; // AND
         case 5 : temp = (op1->read() | op2->read()).to_uint(); break; // OR
         case 6 : temp = (op1->read()).to_uint() - (op2->read()).to_uint(); break; // SUMUB
         case 7 : if ((op1->read()).to_uint() < (op2->read()).to_uint())
                     temp = (1);
                  else
                     temp = (0);
            break;  // SET ON LESS THAN*/
      }

      if(temp == 0)
         zero->write(1);
      else zero->write(0);

      res->write(temp);
   }
}